
# How to write stuff

This is a simple, straightforward way to write good stuff, in a relatively "easy" way.

My example will be a dissertation on "are video games art"

## Step 1 - Ideas

Write the sketch of what you wanna say, in any manner. Better do it on paper, so you can easily move thing around, strike out, draw arrows and so on. Don't worry about structure or logic, just write everything you can think about.

Example:

- "board games"
- "mind games"
- "do animals play? What is the function of play"
- "video games <> physical play, relationship"
- "history of games"
- ...etc

Time: 10mn

## Step 2 - Axis

Choose some sort of narrative. An end. Of course, for a dissertation, this is easy, for example: "yes, they are art". But even for a report, *always find a narrative*. For example, say I am doing a report on a 3D graphics convention I went to. I *still* can find an axis. For example: "this convention is the best place to meet people to hire" or "this conventions is great to find clients".

Time: 2mn

## Step 3 - Reorganize

Take the points you wrote before, and set them as a rough outline that leads to your conclusion. You don't have to set them linearly. For example:

1. "history of games"
  - "board games"
  - "mind games"
2. "physical play"
  - "do animals play?"
  - "What is the function of play"
3. "art - what is the definition?"
4. "theater: is it art? is it play? it's called play a role"
5. ...
6. Finally:"...And so, video games are art"

As you might notice, now that I have a conclusion, I add new points that lead to this conclusion.
Don't be afraid of moving things around a reorganize the thought. Play the succession in your head, and try to see if the transitions make sense.

Time: 20mn

## Step 4 - Research

For a report, this obviously doesn't apply; for a dissertation/story, this is the moment to begin collecting documentation. The documentation will probably trigger new ideas, after which you can repeat the above, if needed.

Time: from a few minutes to years, depending on how deep your research should go

## Step 5 - PUSH IT

You now have some outline, some conclusion. Chances are, it's pretty ok, but also not super interesting. Indeed, what you did in one hour can hardly be as interesting as an article found online, written by an expert who's been working in the field for years. The solution: make it big. Make your conclusion impressive. Make it otherwordly. For example, what if instead of concluding with "video games are art", I concluded with "art doesn't exist?". This is already more interesting. In my example, I'll go with "games are the only art, all other arts depend on games".

Now that you've found your pushed finale, take your previous one and make it the *middle*.

That gives me:

1. something
2. something
3. ...
4. And so, video games are art
5. ?
6. ?
7. ...
8. Therefore: Not only games are art, but games are the only art there is

Now, I *have* to find some way to fill in the blanks. It might seem difficult or impossible, but hey! If it was easy, it would be boring. After scratching my head for a while, here's what I came up with:

1. something
2. something
3. ...
4. And so, video games are art
5. Games are the meta-art:
   1. Games contain all the other arts (music, writing, video, etc), and they add interactivity
6. Games are the original art:
   1. Before there was paintings and music, there was dancing, which was a representation of the hunt, where people "danced" the hunters and their prey
7. All arts started by being interactive
   1. storytelling used to be reactive, with an audience
   2. music used to be participative
   3. theater used to include the audience
8. Therefore: Not only games are art, but games are the only art there is

Now, we're onto something!

It's probably time to reshuffle all of this around. Look at your bullet points, make sure they make sense as they are. Try to replay the flow of the text in your head. You want each main bullet point to convince your reader of something.

What you're looking to do is a conversation where the text tells the reader:
- you agree with point 1? yes? (and the reader should go "of course")
- you agree with point 2? yes? (same)
- ...
- THEREFORE, you *have* to agree with my last point

Time: 10mn to a few hours, depending on inspiration

## Step 6 - Add blanks!

We're going to reproduce what we did in `Step 5`. We're going to add blanks, and force ourselves to fill them. We will basically follow a micro-version of all of the above, for *each bullet point*.

For example:

-  All arts started by being interactive
  - storytelling used to be reactive, with an audience
    - 1.
    - 2.
    - 3.
  - music used to be participative
    - 1.
    - 2.
    - 3.
  - etc

I don't know yet what I need to fill in. I just know that I need to. As for the number of sub-bullet points, put 2, put 3, put 5...It doesn't really matter, what matters is that you add a little more than what is easy. So if you think you can easily find 3 bullet points, put 5. This will force you again to go out of your comfort zone. 

Here's my attempt:

-  All arts started by being interactive
  - storytelling used to be reactive
    - 1. Walking menestrels in the middle ages
    - 2. Puppet shows for kids
    - 3. Even Tv shows now change their scenarios depending on people's reactions
  - music used to be participative
    - 1. In village festivals, people from the village would all play
    - 2. tribal music has everyone contribute (Free Jazz today can be an extension of that)
    - 3. Even good concerts are always in reaction with the people

This was not too hard; I probably should add a 4th bullet point

Time: 30mn

## Step 7 - More Blanks!

Depending on how big your final needs to be, add sub-sub-sub bullet points, following the same principles. You may also add sub-sub-sub-subs-sub bullet points with examples for each point.

Time: 0 to a few hours

## Step 8 - Review

Now is time to make sure your dissertation flows nicely. It's also your last chance to reshuffle around, review, change. You may completely reshuffle the bullet points at that stage. Some deeply nested point can become a main point, and vice-versa. Don't be afraid of moving things around, it's simple and easy still.

Time: 20mn

## Step 9 - Transitions.

Just simply write the transitions between each section. Start with the main transitions, then the sub-transitions, and so on. You may also write the introduction of each bullet point. Don't overcomplicate it! Keep it short and sweet.

-  All arts started by being interactive
   -  `It is a little known fact that art didn't _evolve_ to be interactive. It _was_! Starting with the oldest of all, ...`
  - storytelling used to be reactive
    - `We all can recall the images of prehistoric men and women around a fire...`
    - 1. Walking menestrels in the middle ages
      - `and so, these walking troubadours amused adults. What about kids? Well,`
    - 2. Puppet shows for kids
      - `it is tempting to think that this has changed a lot. Allow me to dispel this idea:`
    - 3. Even Tv shows now change their scenarios depending on people's reactions
      - `Even comic books, or books series react to their audience's emotions. All art is reactive; some just slower than others`

## Step 10 - Fill in the rest

At this point, you have a big outline, references, a good idea of what you want to write, introductions and exits from each part. All that's left to do is... Removing the bullets and actually writing proper sentences.

Congrats! You have an interesting, well thought-out, provocative piece